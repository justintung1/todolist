import React from 'react';


function Editing(props){
  const { handleUpdate, editingTodo, handleEditingChange, cancelEdit} = props;
  function update(e){
    if (e.key === 'Enter') {
      //const { handleUpdate, editingTodo } = props
      handleUpdate(editingTodo.id)
    }
  }
  function change(e){
    //const { handleEditingChange,editingTodo } = props
    handleEditingChange(e.target.value,editingTodo.id)
  }
  //const { editingTodo, cancelEdit } = props;
  return (
    <div id="light" className="editing__area">
      <h1>編輯頁面</h1>
      <hr />
      <h6>Notice：編輯完成按下 enter 即可送出資料</h6>
      <input className="form-control form-control-sm" type="text" placeholder="編輯完成後按下 enter 即可" 
      value={editingTodo.text} onChange={change} onKeyPress={update}/>
      <button type="button" className="editing__cancel btn btn-outline-secondary"  onClick={cancelEdit}>取消編輯</button>
    </div>
  );
}
function Processing() {
  return <div id="fade" className="editing__background" />
}
export { Editing, Processing }





/*

import React ,{useState} from 'react';

class Editing extends React.Component {
  constructor(props) {
    super(props);
    this.update = this.update.bind(this);
    this.change = this.change.bind(this);
  }

  update = (e) => {
    if (e.key === 'Enter') {
      const { handleUpdate, editingTodo } = this.props
      handleUpdate(editingTodo.id)
    }
  }

  change = (e) => {
    const { handleEditingChange,editingTodo } = this.props
    handleEditingChange(e.target.value,editingTodo.id)
  }
  render() {
    const { editingTodo, cancelEdit } = this.props;
    return (
      <div id="light" className="editing__area">
        <h1>編輯頁面</h1>
        <hr />
        <h6>Notice：編輯完成按下 enter 即可送出資料</h6>
        <input className="form-control form-control-sm" type="text"
          placeholder="編輯完成後按下 enter 即可" 
          value={editingTodo.text} onChange={this.change}
          onKeyPress={this.update}/>
        <button type="button" className="editing__cancel btn
          btn-outline-secondary"  onClick={cancelEdit}>取消編輯</button>
      </div>
    );
  }
}
function Processing() {
  return <div id="fade" className="editing__background" />
}
export { Editing, Processing }


*/