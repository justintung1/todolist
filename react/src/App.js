import React, {useState} from 'react';
import './App.css';
import Todo from './Todo.js'
import {Editing,Processing} from './Util.js'

function App(props){
  const [todos, settodos] = useState([]);
  const [todoText, settodoText] = useState('');
  const [editingTodo, seteditingTodo] = useState({});
  const [isEditing, setisEditing] = useState(false);
  const [isProcessing, setisProcessing] = useState(false);
  //var plus=1;
  const [id, setid] = useState(1);
  //let id=1;
  //let now=1;

  function handleSubmit(e){
    settodoText(e.target.value);
  }
  function addTodo(){
    //plus=plus+1;
    settodos(
      [...todos,{
        id:id,
        isCompleted:false,
        text:todoText
      }]
    );
    settodoText(' ');
    setid(id+1);
  }
  function deleteTodo(identity){
    settodos(todos.filter(todo => todo.id !== identity));
  }
  function editTodo(identity,content){
    seteditingTodo({
      id:identity,
      text:content
    });
    setisEditing(true);
    setisProcessing(true);
  }
  function handleEditingChange(content,identity){
    seteditingTodo({
      ...editingTodo,
      text: content,
      id:identity
    });
  }
  function handleUpdate(e){
    settodos(
      todos.map(todo => {
        if(todo.id===e) {
          return {
            ...todo,
            text:editingTodo.text
          }
        }
      return todo;
    }));
    setisProcessing(false);
    setisEditing(false);
    seteditingTodo({});
  }
  function cancelEdit(){
    setisEditing(false);
    setisProcessing(false);
  }

  return (
    <div>
      <div className="todolist__input">
        <h1> todolist </h1>
        <input className="form-control form-control-sm" type="text" name="todo" value={todoText} onChange={handleSubmit}/>
        <button type="button" className="todolist__edit btn btn-outline-secondary" onClick={addTodo}>ADD</button>
        <hr/>
      </div>
      <div className="todolist__list">
        {todos.map(todo => (<Todo todo={todo} key={todo.id} deleteTodo={deleteTodo} editTodo={editTodo}/>))}
      </div>
      {isEditing && <Editing cancelEdit={cancelEdit}
       editingTodo={editingTodo} 
       handleEditingChange={handleEditingChange} 
       handleUpdate={handleUpdate} />}
    </div>
  )
}

export default App;
/*import React, { Component,useState } from 'react';
import './App.css';
import Todo from './Todo.js'
import {Editing,Processing} from './Util.js'
class App extends Component{
  constructor(props){
    super(props);
    this.state={
      todos:[],
      todoText: '',
      editingTodo:{},
      isEditing:false,
      isProcessing:false
    };
    this.id=1;
    this.now=1;
    this.handleSubmit = this.handleSubmit.bind(this);
    this.addTodo = this.addTodo.bind(this);
    this.deleteTodo = this.deleteTodo.bind(this);

    this.editTodo = this.editTodo.bind(this);
    this.handleEditingChange = this.handleEditingChange.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.cancelEdit = this.cancelEdit.bind(this);
    //this.swap = this.swap.bind(this);
  }
  
  handleSubmit(e){
    this.setState({
      todoText:e.target.value,
    })
  }
  addTodo(){
    const { todoText,todos } = this.state;
    //const {swap} = this.props;
    this.setState({
      todos:[...todos,{
        id:this.id,
        isCompleted:false,
        text:todoText
      }],
      todoText:" ",
    })
    this.id++;
  }
  deleteTodo(id){
    const {todos} = this.state;
    this.setState({ 
      todos:todos.filter(todo => todo.id !== id)
    });
  }

  editTodo(identity, content){
    this.setState({
     editingTodo: { id: identity, text: content },
     isEditing: true,
     isProcessing: true
    })
  }
  handleEditingChange(content,identity){
    // 從傳入的改變，id 直接取暫存區的即可。
    const { editingTodo } = this.state
    this.setState({
      editingTodo: {
        ...editingTodo,
        text: content,
        id:identity
      }
    })
  }
  handleUpdate = (e) => {
    const { editingTodo, todos } = this.state
    this.setState({
      todos: todos.map(todo => {
        if(todo.id===e) {
          return {
            ...todo,
            text:editingTodo.text
          }
        }
        return todo;
      }),
      isEditing: false,
      isProcessing: false,
      editingTodo: {},
    })
  }
  cancelEdit = () => {
    this.setState({
      isEditing: false,
      isProcessing: false
    })
  }
  render() {
    const { todos, todoText, isEditing, isProcessing, editingTodo } = this.state
    return (
      <div>
        <div className="todolist__input">
          <h1> todolist </h1>
          <input className="form-control form-control-sm" type="text" name="todo" value={todoText} onChange={this.handleSubmit}/>
          <button type="button" className="todolist__edit btn btn-outline-secondary" onClick={this.addTodo}>ADD</button>
          <hr />
        </div>
      
        <div className="todolist__list">
          {todos.map(todo => (<Todo todo={todo} key={todo.id} deleteTodo={this.deleteTodo} editTodo={this.editTodo}/>))}
        </div>
        {isEditing && <Editing cancelEdit={this.cancelEdit}
         editingTodo={editingTodo} 
         handleEditingChange={this.handleEditingChange} 
         handleUpdate={this.handleUpdate} />}
      </div>
    );
  }
}
export default App;*/

  //https://daveceddia.com/usestate-hook-examples/